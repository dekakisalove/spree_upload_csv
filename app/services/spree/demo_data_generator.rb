require 'csv'

module Spree
  module DemoDataGenerator
    def call
      50.times do
        Taxonomy.create(name: FFaker::Name.unique.name)
      end

      result = []

      100_000.times do
        result << {
          name: FFaker::Name.unique.name,
          description: FFaker::Lorem.paragraph,
          price: rand(1..1000),
          availability_date: Time.zone.at(rand * Time.now.to_i),
          stock_total: rand(10..5000),
          category: Taxonomy.offset(rand(Taxonomy.count)).first.name
        }
      end

      CSV.open('data.csv', 'wb', col_sep: ';') do |csv|
        csv << %w[name description price availability_date stock_total category]

        result.each do |hash|
          csv << hash.values
        end
      end
    end

    module_function :call
  end
end
