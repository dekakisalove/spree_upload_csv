module Spree
  class ProductsImport
    FORMAT_MAPPER = {
      csv: Import::Csv
    }.freeze

    def initialize(headers:, lines:, format: :csv)
      @headers = headers
      @lines = lines

      @format = format
    end

    def call
      raise ArgumentError, 'Invalid format name or format class not exist' if FORMAT_MAPPER[format].nil?

      import_instance = FORMAT_MAPPER[format].new(headers: headers, lines: lines)
      import_instance.call
    end

    private

    attr_reader :headers, :lines, :format
  end
end
