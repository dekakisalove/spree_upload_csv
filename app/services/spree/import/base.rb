module Spree
  module Import
    class Base
      def initialize(headers:, lines:)
        @headers = headers
        @lines = lines

        @logger ||= Logger.new(STDOUT)
      end

      def call
        raise NotImplementedError, 'Please follow the unified interface when adding a new import class, define metohd #call'
      end

      private

      attr_reader :headers, :lines, :logger
    end
  end
end
