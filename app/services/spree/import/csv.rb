require 'csv'

module Spree
  module Import
    class Csv < Spree::Import::Base
      CSV_OPTIONS = {
        headers: true,
        col_sep: ';'
      }.freeze

      ATTRIBUTES_TO_ASSIGN = %w[
        name description price slug
      ].freeze

      def call
        headers = CSV.parse(@headers, CSV_OPTIONS.except(:headers)).flatten.compact

        lines.each do |line|
          row = CSV.parse(line, CSV_OPTIONS.except(:headers)).flatten.compact

          next if row.eql?(headers)

          attributes = Hash[headers.zip(row)].compact

          if product_variant?(attributes)
            import_variant(attributes.except('description', 'slug'))
          else
            import_product(attributes.slice(
                             'name', 'description', 'category', 'price',
                             'availability_date', 'slug', 'stock_total'
                           ))
          end
        end
      end

      def import_product(attributes)
        product = Product.new(attributes.slice(*ATTRIBUTES_TO_ASSIGN))

        product.available_on = attributes['availability_date']
        product.shipping_category = ShippingCategory.find_or_create_by(name: 'Default')

        taxonomy = Taxonomy.find_or_create_by(name: attributes['category'])

        if product.save
          logger.info("Product #{product.name} was created!")

          product.reload

          product.master.stock_items.create(
            stock_location: StockLocation.find_or_create_by(name: :default),
            count_on_hand: attributes['stock_total']
          )

          product.taxons << taxonomy.root
        else
          logger.info(product.errors.full_messages)
        end
      end

      def import_variant(attributes)
        variant = Variant.new

        product = Product.find_by!(slug: attributes['parent_product_slug'])
        option_type = OptionType.find_or_create_by(name: attributes['option_type'], presentation: attributes['option_type'])

        variant.product = product

        variant.assign_attributes(
          attributes.slice(*ATTRIBUTES_TO_ASSIGN)
        )

        variant.cost_price = attributes['price']

        variant.option_values.new(
          name: attributes['option_value'],
          option_type: option_type,
          presentation: attributes['option_value']
        )

        if variant.save
          logger.info("Product Variant #{variant.name} was created!")

          variant.stock_items.create(
            stock_location: StockLocation.find_or_create_by(name: :default),
            count_on_hand: attributes['stock_total']
          )
        else
          logger.info(variant.errors.full_messages)
        end
      end

      private

      def product_variant?(attributes)
        attributes['parent_product_slug'].present?
      end
    end
  end
end
