module Spree
  class ProductFileImportJob < ApplicationJob
    BATCH_SIZE = 50_000

    queue_as :default

    def perform(file_path:, format: :csv)
      raise ArgumentError, 'File does not exist' unless File.exist?(file_path)

      if (File.size(file_path).to_f / 1_024_000) >= 20
        File.open(file_path) do |rows|
          headers = rows.first

          rows.lazy.each_slice(BATCH_SIZE) do |lines|
            ProductsImportJob.perform_now(headers: headers, lines: lines, format: format)
          end
        end
      else
        File.open(file_path) do |rows|
          rows = rows.to_a
          ProductsImportJob.perform_now(headers: rows.first, lines: rows, format: format)
        end
      end
    end
  end
end
