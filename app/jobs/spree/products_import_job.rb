module Spree
  class ProductsImportJob < ApplicationJob
    queue_as :default

    def perform(headers:, lines:, format:)
      import_service = ProductsImport.new(headers: headers, lines: lines, format: format)
      import_service.call
    end
  end
end
