require 'rails_helper'

RSpec.describe Spree::Import::Base, type: :service do
  let(:instance) { described_class.new(headers: headers, lines: lines) }

  let(:headers) { 'title;description' }
  let(:lines) { [] }

  let(:file_path) { 'sample.csv' }

  describe '#call' do
    subject { instance.call }

    it { expect { subject }.to raise_error(NotImplementedError, 'Please follow the unified interface when adding a new import class, define metohd #call') }
  end
end
