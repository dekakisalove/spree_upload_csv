require 'rails_helper'

RSpec.describe Spree::Import::Csv, type: :service do
  let(:instance) { described_class.new(headers: headers, lines: lines) }

  let(:headers) { ';name;description;price;slug;availability_date;stock_total;category' }
  let(:lines) do
    [';Product 1;Demo description;25;product-1;2017-12-04T14:55:22.913Z;10;Bags']
  end

  describe '#call' do
    subject { instance.call }

    it { expect { subject }.to change { Spree::Product.count }.by(1) }

    specify do
      subject

      product = Spree::Product.last

      expect(product.name).to eq 'Product 1'
      expect(product.description).to eq 'Demo description'
      expect(product.price.to_i).to eq 25
    end

    context 'when creating product variant' do
      let(:headers) do
        ';name;description;price;slug;availability_date;stock_total;category;'.concat(
          'parent_product_slug;option_type;option_value'
        )
      end

      let(:lines) do
        [
          ';Product 1;Demo description;25;product-1;2017-12-04T14:55:22.913Z;10;Bags',
          ';Product Variant;Demo description;5;product-2;2017-12-04T14:55:22.913Z;10;Bags;product-1;color;red'
        ]
      end

      it { expect { subject }.to change { Spree::Variant.count }.by(2) }

      specify do
        subject

        variant = Spree::Variant.last

        expect(variant.cost_price.to_i).to eq 5
        expect(variant.product).to eq Spree::Product.find_by(slug: 'product-1')
      end
    end
  end
end
