require 'rails_helper'

RSpec.describe Spree::ProductsImport, type: :service do
  let(:instance) { described_class.new(headers: headers, lines: lines, format: format) }

  let(:headers) { 'title;description' }
  let(:lines) { [] }

  describe '#call' do
    subject { instance.call }

    context 'when csv file format' do
      let(:format) { :csv }

      before { allow_any_instance_of(Spree::Import::Csv).to receive(:call).and_return(true) }

      it { expect { subject }.to_not raise_error }
    end

    context 'when invalid file format' do
      let(:format) { :wrong_format }

      it { expect { subject }.to raise_error(ArgumentError, 'Invalid format name or format class not exist') }
    end
  end
end
